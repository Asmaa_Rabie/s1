<?php
$this->breadcrumbs = array(
    'Users' => array('index'),
    $model->user_id,
);

$this->menu = array(
    array('label' => 'List User', 'url' => array('index')),
    array('label' => 'Create User', 'url' => array('create')),
    array('label' => 'Update User', 'url' => array('update', 'id' => $model->user_id)),
    array('label' => 'Delete User', 'url' => '#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->user_id), 'confirm' => 'Are you sure you want to delete this item?')),
    array('label' => 'Manage User', 'url' => array('admin')),
);
?>

<h1>View User #<?php echo $model->user_id; ?></h1> 

<?php if ((!Yii::app()->user->isGuest)&&($model->user_id!=Yii::app()->user->id)):?>
<div id="follow">
<?php
if (!$isFollowing)
    echo CHtml::ajaxButton('follow', CController::createUrl('user/followUser',array('id' => $model->user_id)),
           array('update'=>'#follow'),
            array('id' => 'send-link-' . uniqid()));
else
    echo CHtml::ajaxButton('unfollow', CController::createUrl('user/unfollowUser', array('id' => $model->user_id)),array('update'=>'#follow'),array('id' => 'send-link-' . uniqid()));
?>
</div>
<?php endif;?>
<?php
$this->widget('zii.widgets.CDetailView', array(
    'data' => $model,
    'attributes' => array(
        'user_id',
        'first_name',
        'last_name',
        'email',
        'password',
        'salt',
        'last_access_date',
        'join_date',
        'profile_picture',
        'type_id',
        'info',
        'last_edit_date',
    ),
));
?>
