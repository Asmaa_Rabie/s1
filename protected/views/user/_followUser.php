<div id="follow"><?php
if(!Yii::app()->user->isGuest)
{
	if (!$isFollowing)
	{
		echo CHtml::ajaxButton('follow', CController::createUrl('user/followUser',array('id' => $id)),
		array('update'=>'#follow'),
		array('id' => 'send-link-' . uniqid()));
	}
	else
	{
		echo CHtml::ajaxButton('unfollow', CController::createUrl('user/unfollowUser', array('id' => $id)),array('update'=>'#follow'),array('id' => 'send-link-' . uniqid()));
	}
}
?></div>
