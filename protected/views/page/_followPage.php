<?php
/**
 * Follow page button.
 *
 * @todo    Place a wall widget here
 * @author  Asmaa
 * @version version: 1.0
 */

?>
<div id="follow">
<?php
if (!$isFollowing)
{

	echo CHtml::ajaxButton(Yii::t('app', 'follow'),
						   CController::createUrl('page/followPage',
												  array(
												  'pageId' => $pageId,
												  'userId' => Yii::app()->user->id
												  )),
						   array(
						   'update' => '#follow'
						   ),
						   array(
						   'id' => 'send-link-' . uniqid()
						   ));
}
else
{
	echo CHtml::ajaxButton(Yii::t('app', 'unfollow'),
						   CController::createUrl('page/unfollowPage',
												  array(
												  'pageId' => $pageId,
												  'userId' => Yii::app()->user->id
												  )),
						   array(
						   'update' => '#follow'
						   ),
						   array(
						   'id' => 'send-link-' . uniqid()
						   ));
}
				 ?>
</div>