<?php
/**
 * Page partial viewer.
 *
 * @author  Asmaa
 * @version version: 1.0
 */

?>
<div id="page-wrapper">
    <?php if (Yii::app()->user->hasFlash('success')) : ?>
        <div class="flash-notice">
            <?php echo Yii::app()->user->getFlash('success'); ?>
        </div>
    <?php endif; ?>
      <?php if (Yii::app()->user->hasFlash('error')) : ?>
    <div class="flash-error"><?php echo Yii::app()->user->getFlash('error'); ?></div>
    <?php endif; ?>
     <?php echo CHtml::image($model->getPageIcon($model->icon), $model->name); ?>
    <h1>
<?php
echo CHtml::link(GxHtml::encode(GxHtml::valueEx($model)),
				 $this->createUrl('view', array(
					  'id' => $model->page_id
					  )));
		?></h1>
<?php
if (PageAdmin::model()->isAdmin($model->page_id, Yii::app()->user->id))
{
	echo "<i>" . Yii::t('app', "Under Group ")
			. CHtml::link(Group::model()->getGroupName($model->group_id),
						  $this
							->createUrl('group/view',
										array(
										'id' => $model->group_id
									))) . "</i>";
	echo "<br />";
	echo CHtml::link(Yii::t('app', "Take this page down"),
					 $this->createUrl('delete', array(
						  'id' => $model->page_id
						  ))) . " | ";
	echo CHtml::ajaxLink(Yii::t('app', "Edit Info"),
						 $this->createUrl('edit', array(
							  'id' => $model->page_id
							  )),
						 array(
						 'update' => '#page-wrapper'
						 ),
						 array(
						 'id' => 'send-link-' . uniqid()
						 ));
	echo "<br />";
	$this->renderPartial('_admins', array(
				'admins' => $admins
			));
}
			   ?>
<?php
if ((!Yii::app()->user->isGuest)
		&& (!PageAdmin::model()->isAdmin($model->page_id, Yii::app()->user->id))) :
		 ?>
        <div id="follow">
<?php
if (!$isFollowing)
		echo CHtml::ajaxButton(Yii::t('app', 'follow'),
							   CController::createUrl('page/followPage',
													  array(
													  'pageId' => $model->page_id,
													  'userId' => Yii::app()->user->id
													  )),
							   array(
							   'update' => '#follow'
							   ),
							   array(
							   'id' => 'send-link-' . uniqid()
							   ));
	else
		echo CHtml::ajaxButton(Yii::t('app', 'unfollow'),
							   CController::createUrl('page/unfollowPage',
													  array(
													  'pageId' => $model->page_id,
													  'userId' => Yii::app()->user->id
													  )),
							   array(
							   'update' => '#follow'
							   ),
							   array(
							   'id' => 'send-link-' . uniqid()
							   ));
						 ?>
        </div>
    <?php endif; ?>
    <div style="border: 1px solid #F0F0F0; padding: 10px;margin: 5px;">
        <i><?php echo $model->info; ?></i>
    </div>
    <?php echo CHtml::image($model->getPageCover($model->cover), $model->name); ?>

    <div style="border: 1px solid #F0F0F0; padding: 10px; margin: 5px;">
<?php
echo CHtml::ajaxLink(Yii::t('app', "Posts"),
					 $this->createUrl('viewPosts', array(
						  'id' => $model->page_id
						  )),
					 array(
					 'update' => '#page-content'
					 ),
					 array(
					 'id' => 'send-link-' . uniqid()
					 ));
echo " | "
		. CHtml::ajaxLink(Yii::t('app', "Followers"),
						  $this
						  ->createUrl('viewFollowers',
									  array(
									  'id' => $model->page_id
								)),
						  array(
						  'update' => '#page-content'
						  ),
						  array(
						  'id' => 'send-link-' . uniqid()
						  ));
echo " | "
		. CHtml::ajaxLink(Yii::t('app', "Photos"),
						  $this
						  ->createUrl('viewPhotos',
									  array(
									  'id' => $model->page_id
								)),
						  array(
						  'update' => '#page-content'
						  ),
						  array(
						  'id' => 'send-link-' . uniqid()
						  ));
echo " | "
		. CHtml::ajaxLink(Yii::t('app', "Calendar"),
						  $this
						  ->createUrl('viewCalendar',
									  array(
									  'id' => $model->page_id
								)),
						  array(
						  'update' => '#page-content'
						  ),
						  array(
						  'id' => 'send-link-' . uniqid()
						  ));
																		?>
    </div>
    <div id="page-content" style="border: 1px solid #F0F0F0; padding: 10px;margin: 5px;">
<?php
$this->renderPartial("page-navigation/_pagePosts", array(
			'model' => $model
		))
																						 ?>
    </div>
</div>