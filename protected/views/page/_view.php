<div class="view">

	<?php echo GxHtml::encode($data->getAttributeLabel('page_id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->page_id), array('view', 'id' => $data->page_id)); ?>
	<br />

	<?php echo GxHtml::encode($data->getAttributeLabel('group_id')); ?>:
	<?php echo GxHtml::link(GxHtml::encode($data->group_id), array('group/view', 'id' => $data->group_id));?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('info')); ?>:
	<?php echo GxHtml::encode($data->info); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('name')); ?>
	<?php echo GxHtml::encode($data->name); ?>
	<br />
        <?php /*
	<?php //echo GxHtml::encode($data->getAttributeLabel('icon')); ?>
	<?php //echo GxHtml::encode($data->icon); ?>
	<br />
	<?php //echo GxHtml::encode($data->getAttributeLabel('cover')); ?>
	<?php //echo GxHtml::encode($data->cover); ?>
	<br />
	<?php //echo GxHtml::encode($data->getAttributeLabel('creation_date')); ?>
	<?php //echo GxHtml::encode($data->creation_date); ?>
	<br />
	<?php /*
	<?php echo GxHtml::encode($data->getAttributeLabel('last_edit_date')); ?>:
	<?php echo GxHtml::encode($data->last_edit_date); ?>
	<br />
         */ ?>
	<?php echo GxHtml::encode($data->getAttributeLabel('type_id')); ?>:
	<?php echo GxHtml::encode($data->type_id); ?>
	<br />
	<?php echo GxHtml::encode($data->getAttributeLabel('creator_id')); ?>:
	<?php echo  GxHtml::link(GxHtml::encode($data->creator_id), array('user/view', 'id' => $data->creator_id));?>
	<br />


</div>