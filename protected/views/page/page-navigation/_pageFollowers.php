<?php
/**
 * @author Asmaa
 * @version version: 1.0
 */
?>
<?php

// Group Members widget to be placed here
echo "<h4>" . Yii::t('app', 'Follower|Followers', count($followers))."</h4>";
if (count($followers) > 0)
{
	echo " (" . count($followers) . ")";
}

if (count($followers) < 1)
{
	echo "<i> No followers yet.</i>";
}

else
{
	foreach ($followers as $follower)
	{
		echo "<div style='padding:5px;margin:5px;border: 1px solid #F0F0F0'>";

		echo CHtml::image($follower->profile_picture, $follower->first_name, array('style' => 'width:64px;height:auto;float:left;'));
		echo "<div>";
		echo CHtml::link($follower->getFullName(), CController::createUrl('user/view', array('id' => $follower->user_id)));

		echo "<br /><i>" . $follower->info . "</i>";
		echo "</div><div class='clear-both'></div>";

		echo "</div>";
	}
}
?>
