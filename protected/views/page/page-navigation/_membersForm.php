<?php
/**
 * @author Asmaa
 * @version version: 1.0
 */
?>
<div id="group-wrapper">
<div class="form"><?php
$form = $this->beginWidget('CActiveForm', array(
            'id' => 'invite-form',
            'enableAjaxValidation' => false,
            'enableClientValidation' => true,
            'clientOptions' => array('validateOnSubmit' => true),
));
?>

<p class="note"><span class="required"> <?php echo Yii::t('app', 'Note that you can only invite people you are following and they are follwing you back too only');?>
</span></p>

<?php echo $form->errorSummary($model); ?>
<div class="row buttons"><?php
foreach ($people as $invited)
{
	echo "<div style='padding:5px;margin:5px;border: 1px solid #F0F0F0'>";
	if (GroupMember::model()->isWaitingApproval($invited->user_id, $model->group_id))
	{
		echo"<i>".Yii::t('app', '(Waiting admin approval)')."</i>";
	}
	elseif (GroupMember::model()->isInvited($invited->user_id, $model->group_id))
	{
		echo"<i>".Yii::t('app', '(Invitation is sent)')."</i>";
	}
	elseif (GroupMember::model()->isAdmin($invited->user_id, $model->group_id))
	{
		echo"<i>".Yii::t('app', '(Admin)')."</i>";
	}
	elseif (GroupMember::model()->isMember($invited->user_id, $model->group_id))
	{
		echo"<i>".Yii::t('app', '(Member)')."</i>";
	}

	else
	echo CHtml::checkBox($invited->user_id, $select);
	echo CHtml::image($invited->profile_picture, $invited->first_name, array('style' => 'width:64px;height:auto;float:left;')) .
                "<div>" . CHtml::link($invited->first_name . " " . $invited->last_name, CController::createUrl('user/view',array('id'=>$invited->user_id))) . "<br /><i>" . $invited->info . "</i></div><div class='clear-both'></div>";

	echo "</div>";
}
//            echo CHtml::checkBoxList('invited', $select, $nas, array('separator' => "  ",
//                'template' => "<div style='padding:5px;margin:5px;border: 1px solid #F0F0F0'><div style='float:left' >{input}</div>{label}</div><div class='clear-both'></div></div>"
//            ));
?></div>
<div class="row buttons"><?php
echo CHtml::ajaxSubmitButton(Yii::t('app', 'Invite Selected'), array('invite', 'id' => $model->group_id), array('update' => '#group-wrapper', 'data' => 'js:$("#invite-form").serialize()', 'method' => 'post')); //CHtml::submitButton('Invite Selected');//CHtml::ajaxSubmitButton('Invite Selected', $this->createUrl('invitePeople', array('id' => $model->group_id)), array('update' => '#group-wrapper'));
?></div>
<?php $this->endWidget(); ?></div>
<!-- form --></div>
