<?php
/**
 * @author Asmaa
 * @version version: 1.0 
 */ 
?>
<div id="group-wrapper">
    <h4><?php echo Yii::t('app', 'Invitation Approval Requests')?></h4>
    
    <?php
    if (count($invReqs)==0)
    {
        echo Yii::t('app', 'No pending requests to show');
    }
    else
    {
        foreach ($invReqs as $request)
        {
            echo "<div style='border: 1px solid #F0F0F0; padding: 10px;margin: 5px;' id='request-" . $request->user_id . "'>";
            echo CHtml::link($request->first_name . " " . $request->last_name
                    , CController::createUrl('user/view', array('id' => $request->user_id)));

            echo CHtml::ajaxButton(Yii::t('app', 'Approve')
                    , $this->createUrl('approveInv', array('groupid' => $groupid, 'userid' => $request->user_id))
                    , array(
                'success' => 'function(){$("#request-' . $request->user_id . '").fadeOut("slow");}'
            ));
            echo CHtml::ajaxButton(Yii::t('app', 'Cancel Request')
                    , $this->createUrl('cancelInv', array('groupid' => $groupid, 'userid' => $request->user_id))
                    , array(
                'success' => 'function(){$("#request-' . $request->user_id . '").fadeOut("medium");}'
            ));
            echo "</div>";
        }
    }
    
    ?>
</div>