<?php
/**
 * Page view.
 *
 * @author  Asmaa
 * @version version: 1.0
 */

?>
<?php
$this->breadcrumbs = array(
	$model->label(2) => array(
		'index'
	), GxHtml::valueEx($model),
);

$this->menu = array(
		array(
			'label' => Yii::t('app', 'List') . ' ' . $model->label(2), 'url' => array(
				'index'
			)
		),
		array(
			'label' => Yii::t('app', 'Create') . ' ' . $model->label(), 'url' => array(
				'create'
			)
		),
		array(
				'label' => Yii::t('app', 'Update') . ' ' . $model->label(),
				'url' => array(
					'update', 'id' => $model->page_id
				)
		),
		/* array('label' => Yii::t('app', 'Delete') . ' ' . $model->label(), 'url' =>
		 *  '#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->page_id),
		 *   'confirm' => 'Are you sure you want to delete this item?')),*/
		array(
			'label' => Yii::t('app', 'Manage') . ' ' . $model->label(2), 'url' => array(
				'admin'
			)
		),
);
  ?>
<!-- Mine PART -->
<?php

$this
		->renderPartial('_pageView',
						array(
						'model' => $model, 'admins' => $admins, 'isFollowing' => $isFollowing,
						));
				  ?>
