<?php
/**
 * Page update/create form.
 *
 * @author  Asmaa
 * @version version: 1.0
 */

?>
<div id="page-wrapper">
    <div class="form">
<?php
if (!$model->isNewRecord)
{
	echo "<h1>" . Yii::t('app', 'Edit') . "</h1>";
	echo CHtml::ajaxLink(Yii::t('app', '&larr; Back to the page'),
						 $this
						 ->createUrl('view', array(
						 'id' => $model->page_id
						 )),
						 array(
						 'update' => '#page-wrapper'
						 ),
						 array(
						 'id' => 'send-link-' . uniqid()
						 ));
	echo "<br />";
}
					  ?>
<?php
$form = $this
		->beginWidget('GxActiveForm',
					  array(
					  'id' => 'page-form', 'enableAjaxValidation' => false,
					  ));
						?>

        <p class="note">
            <?php echo Yii::t('app', 'Fields with'); ?>
             <span class="required">*</span>
             <?php echo Yii::t('app', 'are required'); ?>.
        </p>

        <?php echo $form->errorSummary($model); ?>

        <div class="row">
            <?php if ($model->scenario == 'create') : ?>
                  <?php echo $form->labelEx($model, 'group_id'); ?>
                  <?php echo $form
			->dropDownList($model, 'group_id', GxHtml::listDataEx(User::model()->getAdminGroups()));
																  ?>
                  <?php echo $form->error($model, 'group_id'); ?>
              <?php endif; ?>
        </div><!-- row -->
        <div class="row">
            <?php echo $form->labelEx($model, 'info'); ?>
              <?php echo $form->textField($model, 'info'); ?>
              <?php echo $form->error($model, 'info'); ?>
        </div><!-- row -->
        <div class="row">
            <?php echo $form->labelEx($model, 'name'); ?>
<?php
echo $form
		->textField($model, 'name', array(
			'maxlength' => 255
		));
														 ?>
              <?php echo $form->error($model, 'name'); ?>
        </div><!-- row -->
        <?php
        /*
        <div class="row">
            <?php //echo $form->labelEx($model,'icon'); ?>
              <?php //echo $form->textField($model, 'icon', array('maxlength' => 255)); ?>
              <?php //echo $form->error($model,'icon');  ?>
        </div><!-- row -->
        <div class="row">
            <?php //echo $form->labelEx($model,'cover'); ?>
              <?php // echo $form->textField($model, 'cover', array('maxlength' => 255)); ?>
              <?php //echo $form->error($model,'cover');  ?>
        </div><!-- row -->
	`	*/
        ?>
        <div class="row">
            <?php if ($model->scenario == 'create') : ?>
                  <?php echo $form->labelEx($model, 'type_id'); ?>
<?php
echo $form
			->dropDownList($model,
						   'type_id',
						   GxHtml::listDataEx(PageType::model()->findAllAttributes(null, true)));
																 ?>
                  <?php echo $form->error($model, 'type_id'); ?>
              <?php endif; ?>
        </div><!-- row -->

<?php
if ($model->isNewRecord)
	echo GxHtml::submitButton(Yii::t('app', 'Launch Now!'));
else
	echo CHtml::ajaxSubmitButton(Yii::t('app', 'Save'),
								 $this
								 ->createUrl('edit',
											 array(
											 'id' => $model->page_id
								 )),
								 array(
								 'update' => '#page-wrapper'
								 ),
								 array(
								 'id' => 'send-link-' . uniqid()
								 ));

$this->endWidget();
?>
    </div><!-- form -->
</div>