<?php
/**
 * Page administrators inside page.
 *
 * @author  Asmaa
 * @version version: 1.0
 */

?>
<div style='border: 1px solid #F0F0F0; padding: 10px;margin: 5px;'>
    <h4><?php echo Yii::t('app', 'Admins'); ?></h4>
<?php
foreach ($admins as $admin)
{
	echo CHtml::link($admin->getFullName(),
					 $this->createUrl('user/view', array(
						  'id' => $admin->user_id
						  )));
	echo "<br />";
}
				   ?>
</div>