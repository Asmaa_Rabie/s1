<?php
/**
 * Invite people form inside the group.
 *
 * @author  Asmaa
 * @version version: 1.0
 */

?>
<div id="group-wrapper">
<?php
echo "<h1>Invite People</h1>";
echo CHtml::ajaxLink('&larr; Back to the group',
					 CController::createUrl('group/view', array(
					 'id' => $model->group_id
					 )),
					 array(
					 'update' => '#group-wrapper'
					 ),
					 array(
					 'id' => 'send-link-' . uniqid()
					 ));
echo "<br /><br />";

$this
		->renderPartial('group-navigation/_membersForm',
						array(
						'model' => $model, 'people' => $people
						));
						?>

</div>