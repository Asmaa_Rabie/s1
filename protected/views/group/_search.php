<?php
/**
 * Group default search form.
 *
 * @todo    Place a wall widget here
 * @author  Yii
 * @version version: 1.0
 */

?>
<div class="wide form">

<?php $form = $this->beginWidget('GxActiveForm', array(
	'action' => Yii::app()->createUrl($this->route),
	'method' => 'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model, 'group_id'); ?>
		<?php echo $form->textField($model, 'group_id', array('maxlength' => 10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'name'); ?>
		<?php echo $form->textField($model, 'name', array('maxlength' => 255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'type_id'); ?>
		<?php echo $form->dropDownList($model, 'type_id', GxHtml::listDataEx(GroupType::model()->findAllAttributes(null, true)), array('prompt' => Yii::t('app', 'All'))); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'creator_id'); ?>
		<?php echo $form->dropDownList($model, 'creator_id', GxHtml::listDataEx(User::model()->findAllAttributes(null, true)), array('prompt' => Yii::t('app', 'All'))); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'creation_date'); ?>
		<?php echo $form->textField($model, 'creation_date', array('maxlength' => 10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'last_edit_date'); ?>
		<?php echo $form->textField($model, 'last_edit_date', array('maxlength' => 10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'tasks_enabled'); ?>
		<?php echo $form->textField($model, 'tasks_enabled'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'pages_enabled'); ?>
		<?php echo $form->textField($model, 'pages_enabled'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'calendar_enabled'); ?>
		<?php echo $form->textField($model, 'calendar_enabled'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'cover'); ?>
		<?php echo $form->textField($model, 'cover', array('maxlength' => 255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'icon'); ?>
		<?php echo $form->textField($model, 'icon', array('maxlength' => 255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model, 'info'); ?>
		<?php echo $form->textField($model, 'info'); ?>
	</div>

	<div class="row buttons">
		<?php echo GxHtml::submitButton(Yii::t('app', 'Search')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
