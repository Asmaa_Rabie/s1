<?php
/**
 * Group partial viewer inside the group.
 *
 * @todo    Place a wall widget here
 * @author  Asmaa
 * @version version: 1.0
 */

?>
<div id="group-wrapper">
    <?php if (Yii::app()->user->hasFlash('success')) : ?>
        <div class="flash-notice">
            <?php echo Yii::app()->user->getFlash('success'); ?>
        </div>
    <?php endif; ?>
      <?php if (Yii::app()->user->hasFlash('error')) : ?>
    <div class="flash-error"><?php echo Yii::app()->user->getFlash('error'); ?></div>
    <?php endif; ?>
<div style="border: 1px solid #F0F0F0; padding: 10px; margin: 5px;">
<?php
if (GroupMember::model()->isInvited(Yii::app()->user->id, $model->group_id))
{
	echo CHtml::link(Yii::t('app', "Accept Invitation"),
					 $this
					 ->createUrl('acceptInv', array(
						'id' => $model->group_id
					 )));
	echo " | ";
	echo CHtml::link(Yii::t('app', "Decline Invitation"),
					 $this
					 ->createUrl('declineInv', array(
						'id' => $model->group_id
					 )));
}
																	?> <br />
<?php echo CHtml::image($model->icon, $model->name); ?>
<h1>
<?php
echo CHtml::link(GxHtml::encode(GxHtml::valueEx($model)),
				 $this->createUrl('view', array(
					  'id' => $model->group_id
					  )));
	?></h1>
<?php
if (!GroupMember::model()->isAdmin(Yii::app()->user->id, $model->group_id))
{
	echo CHtml::link(Yii::t('app', "Leave Group"),
					 $this
					 ->createUrl('declineInv', array(
						'id' => $model->group_id
					 ))) . $type->name;
}
	   ?>
</i> <br />
<i><?php echo $model->info; ?></i>
<?php
if ($model->pages_enabled)
{
	echo Yii::t('app', "This group has it's public page enabled ");
	echo CHtml::link(Yii::t('app', 'See how people see you'),
					 CController::createUrl('page/view',
											array(
											'id' => $model->pages[0]->page_id
					)));
}
								  ?> <br />
<?php echo CHtml::image($model->cover, $model->name); ?></div>
<?php if (!GroupMember::model()->isInvited(Yii::app()->user->id, $model->group_id)) : ?>
<div style="border: 1px solid #F0F0F0; padding: 10px; margin: 5px;"
	id="group-content-wrapper">
<?php
	if (GroupMember::model()->isAdmin(Yii::app()->user->id, $model->group_id))
	{
		echo CHtml::ajaxLink(Yii::t('app', "Info & Settings"),
							 $this
							 ->createUrl('edit',
										 array(
										 'id' => $model->group_id
								)),
							 array(
							 'update' => '#group-wrapper'
							 ),
							 array(
							 'id' => 'send-link-' . uniqid()
							 )) . " | ";
		echo CHtml::link(Yii::t('app', "Take this group down"),
						 $this
						 ->createUrl('delete',
									 array(
									 'id' => $model->group_id
								))) . " | ";
	}
	echo CHtml::ajaxLink(Yii::t('app', "Invite People"),
						 $this
						 ->createUrl('viewInvitePeople',
									 array(
									 'id' => $model->group_id
							)),
						 array(
						 'update' => '#group-wrapper'
						 ),
						 array(
						 'id' => 'send-link-' . uniqid()
						 ));
							   ?>
<div id="stat">
<?php
	echo CHtml::ajaxLink($postsCount . Yii::t('app', ' Post| Posts', $postsCount),
						 $this
						 ->createUrl('viewPosts',
									 array(
									 'id' => $model->group_id
							)),
						 array(
						 'update' => '#group-content'
						 ),
						 array(
						 'id' => 'send-link-' . uniqid()
						 ));
	echo " | "
			. CHtml::ajaxLink($filesCount . Yii::t('app', ' File| Files', $filesCount),
							  $this
							  ->createUrl('viewFiles',
										  array(
										  'id' => $model->group_id
									)),
							  array(
							  'update' => '#group-content'
							  ),
							  array(
							  'id' => 'send-link-' . uniqid()
							  ));
	echo " | "
			. CHtml::ajaxLink($membersCount . Yii::t('app', ' Member| Members', $membersCount),
							  $this
							  ->createUrl('viewMembers',
										  array(
										  'id' => $model->group_id
									)),
							  array(
							  'update' => '#group-content'
							  ),
							  array(
							  'id' => 'send-link-' . uniqid()
							  ));
			   ?></div>

<div style="border: 1px solid #F0F0F0; padding: 10px; margin: 5px;">
<?php
	if ($model->pages_enabled)
		echo CHtml::ajaxLink(Yii::t('app', "Public Wall"),
							 CController::createUrl('page/viewPosts',
													array(
													'id' => $model->pages[0]->page_id
						)),
							 array(
							 'update' => '#group-content'
							 ),
							 array(
							 'id' => 'send-link-' . uniqid()
							 )) . " | ";
	echo CHtml::ajaxLink(Yii::t('app', "Private Wall"),
						 $this
						 ->createUrl('viewPosts',
									 array(
									 'id' => $model->group_id
							)),
						 array(
						 'update' => '#group-content'
						 ),
						 array(
						 'id' => 'send-link-' . uniqid()
						 ));
	echo " | "
			. CHtml::ajaxLink(Yii::t('app', "Files"),
							  $this
							  ->createUrl('viewFiles',
										  array(
										  'id' => $model->group_id
									)),
							  array(
							  'update' => '#group-content'
							  ),
							  array(
							  'id' => 'send-link-' . uniqid()
							  ));
	echo " | "
			. CHtml::ajaxLink(Yii::t('app', "Members"),
							  $this
							  ->createUrl('viewMembers',
										  array(
										  'id' => $model->group_id
									)),
							  array(
							  'update' => '#group-content'
							  ),
							  array(
							  'id' => 'send-link-' . uniqid()
							  ));
	/*
	echo " | "
	        . CHtml::ajaxLink(Yii::t('app', "Photos"),
	                          $this->createUrl('viewPhotos', array(
	                               'id' => $model->group_id
	                               )),
	                          array(
	                          'update' => '#group-content'
	                          ),
	                          array(
	                          'id' => 'send-link-' . uniqid()
	                          ));
	 */

	if ($model->tasks_enabled)
		echo " | "
				. CHtml::ajaxLink(Yii::t('app', "Tasks"),
								  $this
								  ->createUrl('viewTasks',
											  array(
											  'id' => $model->group_id
										)),
								  array(
								  'before' => CHtml::image(Yii::app()->baseUrl
														   . '/images/system/ajax-loader.gif',
														   'loading...'),
								  'update' => '#group-content'
								  ),
								  array(
								  'id' => 'send-link-' . uniqid()
								  ));

	if ($model->calendar_enabled)
		echo " | "
				. CHtml::ajaxLink(Yii::t('app', "Calendar"),
								  $this
								  ->createUrl('viewCalendar',
											  array(
											  'id' => $model->group_id
										)),
								  array(
								  'update' => '#group-content'
								  ),
								  array(
								  'id' => 'send-link-' . uniqid()
								  ));
	if (GroupMember::model()->isAdmin(Yii::app()->user->id, $model->group_id))
		echo " | "
				. CHtml::ajaxLink(Yii::t('app', "Pending Requests"),
								  $this
								  ->createUrl('viewRequests',
											  array(
											  'id' => $model->group_id
										)),
								  array(
								  'update' => '#group-content'
								  ),
								  array(
								  'id' => 'send-link-' . uniqid()
								  ));
																	?></div>
<div id="group-content"
	style="border: 1px solid #F0F0F0; padding: 10px; margin: 5px;">
<?php
$this->renderPartial("group-navigation/_groupPosts", array(
				'model' => $model
			));
?>
</div>
</div>
<?php endif; ?>
</div>