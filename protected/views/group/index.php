<?php
/**
 * Group default index.
 *
 * @author  Asmaa
 * @version version: 1.0
 */

?>
<?php
$this->breadcrumbs = array(
	Group::label(2), Yii::t('app', 'Index'),
);

$this->menu = array(
		array(
			'label' => Yii::t('app', 'Create') . ' ' . Group::label(), 'url' => array(
				'create'
			)
		),
		array(
			'label' => Yii::t('app', 'Manage') . ' ' . Group::label(2), 'url' => array(
				'admin'
			)
		),
);
  ?>

<h1><?php echo GxHtml::encode(Group::label(2)); ?></h1>
<?php $this
		->widget('zii.widgets.CListView',
				 array(
					'dataProvider' => $dataProvider, 'itemView' => '_view',
				 ));
