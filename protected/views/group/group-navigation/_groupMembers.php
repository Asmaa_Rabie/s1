<?php
/**
 * Group members viewer inside the group.
 *
 * @author  Asmaa
 * @version version: 1.0
 */

?>
<?php

// Group Members widget to be placed here .
echo "<h4>" . Yii::t('app', 'Member|Members', count($members)) . "</h4>";
foreach ($members as $member)
{

	echo "<div style='padding:5px;margin:5px;border: 1px solid #F0F0F0'>";

	echo CHtml::image($member->profile_picture,
					  $member->first_name,
					  array(
					  'style' => 'width:64px;height:auto;float:left;'
					  ));
	echo "<div>";
	echo CHtml::link($member->first_name . " " . $member->last_name,
					 CController::createUrl('user/view', array(
					 'id' => $member->user_id
					 )));
	if (GroupMember::model()->isAdmin($member->user_id, $groupId))
	{
		echo "<i>" . Yii::t('app', '(Admin)') . "</i>";
	}
	elseif (GroupMember::model()->isAdmin(Yii::app()->user->id, $groupId))
	{
		/*
		//1. remove from group -ajax
		//2. set as admin - ajax
		//echo "<span id ='admin-".$member->user_id."'>";
		//        echo CHtml::link (Yii::t('app', 'set him as admin')
		//        , $this->createUrl('setAdmin',
		 * array('grp_id'=>$groupId, 'user_id'=>$member->user_id)));
		//       // echo "<span id='admin-".$member->user_id."'>";
		//        echo CHtml::ajaxButton(Yii::t('app', 'set him as admin')
		//        , $this->createUrl('setAdmin',
		 * array('grp_id'=>$groupId, 'user_id'=>$member->user_id))
		//        ,array('success' =>
		 * 'function(){$("#admin-' . $member->user_id . '").html("<i>(Admin)</i>");}')
		//
		//                );
		// echo "</span>";
		 *
		 */
	}
	echo "<br /><i>" . $member->info . "</i>";
	echo "</div><div class='clear-both'></div>";

	echo "</div>";
}
  ?>
