<?php
/**
 * Group pages viewer inside the group.
 *
 * @todo    To be edited later for premium subscribers
 * @author  Asmaa
 * @version version: 1.0
 */

?>
<?php

// Pages widget to be placed here.
if (count($pages) == 0)
{
	echo Yii::t('app', 'This group has no pages yet      ');

}
else
{
	foreach ($pages as $page)
	{
		echo "<div style='border: 1px solid #F0F0F0; padding: 10px;margin: 5px;'>";
		echo CHtml::image($page->icon, $page->name);
		echo CHtml::link($page->name,
						 $this
						 ->createUrl('page/view', array(
							'id' => $page->page_id
						 )));
		echo "<br />";
		echo "<i>" . $page->info . "</i>";
		echo "</div>";
		echo "<br />";
	}

}
echo "<br />";
echo CHtml::link(Yii::t('app', 'Create page now!'),
				 $this->createUrl('page/create', array(
					  'groupId' => $groupId
					  )));
  ?>
