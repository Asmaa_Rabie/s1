<?php
/**
 * Group create / update form.
 *
 * @author  Asmaa
 * @version version: 1.0
 */

?>
<div id="group-wrapper">
<?php
if (!$model->isNewRecord)
{
	echo "<h1>" . Yii::t('app', 'Edit') . "</h1>";
	echo CHtml::ajaxLink(Yii::t('app', '&larr; Back to the group'),
						 $this
						 ->createUrl('view', array(
						 'id' => $model->group_id
						 )),
						 array(
						 'update' => '#group-wrapper'
						 ),
						 array(
						 'id' => 'send-link-' . uniqid()
						 ));
	echo "<br />";
}
						?>
    <div class="form">
<?php
$form = $this
		->beginWidget('GxActiveForm',
					  array(
						'id' => 'group-form',
						'enableAjaxValidation' => false,
						'focus' => array(
							$model, 'name'
						),
						'enableClientValidation' => true,
						'clientOptions' => array(
							'validateOnSubmit' => true
						),
					  ));
					  ?>

<p class="note">
<?php echo Yii::t('app', 'Fields with'); ?>
<span class="required">*</span>
<?php echo Yii::t('app', 'are required'); ?>.
</p>

        <?php echo $form->errorSummary($model); ?>

        <div class="row">
            <?php echo $form->labelEx($model, 'name'); ?>
<?php
echo $form->textField($model, 'name', array(
			'maxlength' => 255
		  ));
														 ?>
    <?php echo $form->error($model, 'name'); ?>
        </div><!-- row -->
        <div class="row">
            <?php if ($model->scenario == 'edit') : ?>
                                  <?php echo $form->labelEx($model, 'info'); ?>
                                  <?php echo $form->textArea($model, 'info'); ?>
                                  <?php echo $form->error($model, 'info'); ?>
                <?php endif; ?>
        </div>
        <div class="row">
            <?php if ($model->scenario == 'create') : ?>
                                  <?php echo $form->labelEx($model, 'type_id'); ?>
<?php
$typeNames = array();
$typeIcons = array();
																				 ?>
                <div id="group-type-icon-container">
<?php
	foreach ($types as $type)
	{
		$typeNames[$type->type_id] = CHtml::image($type->icon,
												  $type->name,
												  array(
												  'class' => 'type-icon'
												  )) . '<div class="type-label">' . $type->name
				. '</div>';
	}
	echo $form
			->radioButtonList($model,
							  'type_id',
							  $typeNames,
							  array(
							  'separator' => "  ",
							  'template' => "<div class='group-type'>
						  {label}<div class='type-radio'>{input}</div></div>",
							  ));
													?>
                </div>
                <div class="clear-both"></div>
<?php endif; ?>
        </div>
        <div class="row">
<?php if ($model->scenario == 'edit') : ?>
                                  <?php echo $form->labelEx($model, 'tasks_enabled'); ?>
                                  <?php echo $form->checkBox($model, 'tasks_enabled'); ?>
                                  <?php echo $form->error($model, 'tasks_enabled'); ?>
  <?php endif; ?>
        </div><!-- row -->
        <div class="row">
<?php if ($model->scenario == 'edit' && !$model->pages_enabled) : ?>
                                  <?php echo $form->labelEx($model, 'pages_enabled'); ?>
                                  <?php echo $form->checkBox($model, 'pages_enabled'); ?>
                                  <?php echo $form->error($model, 'pages_enabled'); ?>
  <?php endif; ?>
        </div><!-- row -->
        <div class="row">
            <?php if ($model->scenario == 'edit') : ?>
                                  <?php echo $form->labelEx($model, 'calendar_enabled'); ?>
                                  <?php echo $form->checkBox($model, 'calendar_enabled'); ?>
                                  <?php echo $form->error($model, 'calendar_enabled'); ?>
                              <?php endif; ?>
        </div><!-- row -->
          <div class="row">
            <?php if ($model->scenario == 'edit') : ?>
                                  <?php echo $form->labelEx($model, 'ads_enabled'); ?>
                                  <?php echo $form->checkBox($model, 'ads_enabled'); ?>
                                  <?php echo $form->error($model, 'ads_enabled'); ?>
                              <?php endif; ?>

<?php
/*
<div class="row">
    <?php //echo $form->labelEx($model,'cover'); ?>
    <?php //echo $form->textField($model, 'cover', array('maxlength' => 255));  ?>
   <?php //echo $form->error($model,'cover');  ?>
</div><!-- row -->
<div class="row">
    <?php //echo $form->labelEx($model,'icon'); ?>
    <?php //echo $form->textField($model, 'icon', array('maxlength' => 255));  ?>
    <?php //echo $form->error($model,'icon');  ?>
</div><!-- row -->

<label><?php // echo GxHtml::encode
($model->getRelationLabel('users'));               ?></label>
<?php // echo $form->checkBoxList($model, 'users',
GxHtml::encodeEx(GxHtml::listDataEx(User::model()->
findAllAttributes(null, true)), false, true)); ?>
<label><?php // echo GxHtml::encode($model->
getRelationLabel('pages'));               ?></label>
<?php // echo $form->checkBoxList($model, 'pages',
GxHtml::encodeEx(GxHtml::listDataEx(Page::model()->
findAllAttributes(null, true)), false, true)); ?>
<label><?php // echo GxHtml::encode($model->
getRelationLabel('tasks'));               ?></label>
<?php // echo $form->checkBoxList($model, 'tasks',
 GxHtml::encodeEx(GxHtml::listDataEx(Task::model()->
 findAllAttributes(null, true)), false, true)); ?>
 */
											?>
<?php
if ($model->isNewRecord)
{
	echo GxHtml::submitButton(Yii::t('app', 'Create'));
}
else
{
	echo CHtml::ajaxSubmitButton(Yii::t('app', 'Save'),
								 $this
								 ->createUrl('edit',
											 array(
											 'id' => $model->group_id
								 )),
								 array(
								 'update' => '#group-wrapper'
								 ),
								 array(
								 'id' => 'send-link-' . uniqid()
								 ));
}
$this->endWidget();
  ?>
</div><!-- form -->
</div>
</div>