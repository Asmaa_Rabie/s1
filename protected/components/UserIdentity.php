<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	private $_id;
	public $email;
	public $model;
	const ERROR_EMAIL_INVALID=3;

	/**
	 * Constructor.
	 * @param string $email username
	 * @param string $password password
	 */
	public function __construct($email,$password)
	{
		$this->email=$email;
		$this->password=$password;
	}
	/**
	 * Authenticates a user.
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate()
	{
		$user=UserProfile::model()->find('LOWER(email)=?',array(strtolower($this->email)));
		if($user===null)
		$this->errorCode=UserIdentity::ERROR_EMAIL_INVALID;
		else if(!$user->validatePassword($this->password))
		$this->errorCode=self::ERROR_PASSWORD_INVALID;
		else
		{
			$this->storeUserDetails($user);
		}
		return $this->errorCode==self::ERROR_NONE;
	}

	public function storeUserDetails($model){
		$this->_id=$model->user_id;
		$this->email=$model->email;
		$this->model=$model;
		$this->errorCode=self::ERROR_NONE;
	}
	/**
	 * @return integer the ID of the user record
	 */
	public function getId()
	{
		return $this->_id;
	}

	/**
	 * @return integer the ID of the user record
	 */
	public function getName()
	{
		return $this->model->first_name . ' '. $this->model->last_name ;
	}

	/**
	 * @return integer the ID of the user record
	 */
	public function getModel()
	{
		return $this->model;
	}
}