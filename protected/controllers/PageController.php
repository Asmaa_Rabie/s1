<?php
/**
 * Page Controller.
 *
 * @author  Asmaa
 * @version version:1.0
 * @since   version 1.0
 */

/**
 * Page Controller Class.
 *
 * @author  Asmaa
 * @version version:1.0
 * @since   version 1.0
 */
class PageController extends GxController
{

	/**
	 * Filters
	 *
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations.
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 *
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
				array(
						'allow',
						// allow all users to perform 'index' and 'view' actions.
						'actions' => array(
							'index', 'view'
						),
						'users' => array(
							'*'
						),
				),
				array(
						'allow',
						// allow authenticated user to perform 'create' and 'update' actions.
						'actions' => array(
								'create',
								'update',
								'followPage',
								'unfollowPage',
								'viewCalendar',
								'viewPosts',
								'viewFollowers',
								'viewPhotos',
								'delete',
								'edit'
						),
						'users' => array(
							'@'
						),
				),
				array(
						'allow',
						// allow admin user to perform 'admin' and 'delete' actions.
						'actions' => array(
							'admin'
						),
						'users' => array(
							'admin'
						),
				),
				array(
					'deny', // deny all users.
					'users' => array(
						'*'
					),
				),
		);
	}
	/**
	 * Rendering the page view and all of its related data
	 *
	 * @author Asmaa
	 * @param integer $id Id of the page to be viewed
	 * @return void
	 */
	public function actionView($id)
	{
		if (Yii::app()->getRequest()->getIsAjaxRequest())
		{
			$this
					->renderPartial('_pageView',
									array(
									'model' => $this->loadModel($id, 'Page'),
									'admins' => PageAdmin::model()->getPageAdmins($id),
									'isFollowing' => (!Yii::app()->user->isGuest)
											? UserFollowPage::model()
													->isFollowingPage($id, Yii::app()->user->id)
											: false,
									),
									false,
									true);
		}
		else
		{
			$this
					->render('view',
							 array(
									'model' => $this->loadModel($id, 'Page'),
									'admins' => PageAdmin::model()->getPageAdmins($id),
									'isFollowing' => (!Yii::app()->user->isGuest)
											? UserFollowPage::model()
													->isFollowingPage($id, Yii::app()->user->id)
											: false,
							 ));
		}
	}

	/**
	 * Displays a particular page's posts.
	 *
	 * @param integer $id Id of the page to be viewed
	 * @author Asmaa
	 * @return void
	 */
	public function actionViewPosts($id)
	{
		$data = array();
		$data["model"] = $this->loadModel($id, 'Page');
		$this->renderPartial('page-navigation/_pagePosts', $data, false, true);
	}

	/**
	 * Displays a particular page's followers
	 *
	 * @param integer $id Id of the page to be viewed
	 * @author Asmaa
	 * @return void
	 */
	public function actionViewFollowers($id)
	{
		$data["followers"] = UserFollowPage::model()->getPageFollowers($id);
		$data["pageid"] = $id;
		$this->renderPartial('page-navigation/_pageFollowers', $data, false, true);
	}

	/**
	 * Displays a particular page's photo album.
	 *
	 * @param integer $id Id of the page to be viewed
	 * @author Asmaa
	 * @return void
	 */
	public function actionViewPhotos($id)
	{

		$data = array();
		$data["model"] = $this->loadModel($id, 'Page');
		$this->renderPartial('page-navigation/_pagePhotos', $data, false, true);
	}

	/**
	 * Displays a particular page's calendar.
	 *
	 * @param integer $id Id of the page to be viewed
	 * @author Asmaa
	 * @return void
	 */
	public function actionViewCalendar($id)
	{
		$data = array();
		$data["model"] = $this->loadModel($id, 'Page');
		$this->renderPartial('page-navigation/_pageCalendar', $data, false, true);
	}

	/**
	 * Create a page
	 *
	 * @param integer $groupId Group id that the newely created page belongs to
	 * @author Asmaa
	 * @return void
	 */
	public function actionCreate($groupId = null)
	{
		$model = new Page;
		$model->scenario = 'create';
		if (!($groupId == null))
		{
			$model->group_id = $groupId;
		}

		if (isset($_POST['Page']))
		{
			$model->setAttributes($_POST['Page']);
			$model->creator_id = Yii::app()->user->id;
			if ($model->save())
			{
				if (Yii::app()->getRequest()->getIsAjaxRequest())
					Yii::app()->end();
				else
				{
					if (PageAdmin::model()->addAdmin($model->page_id, $model->creator_id))
						$this->redirect(array(
									'view', 'id' => $model->page_id
								));
				}
			}
		}

		$this->render('create', array(
					'model' => $model
				));
	}

	/**
	 * Editing page's data
	 *
	 * @param integer $id Id of the page to be edited
	 * @author Asmaa
	 * @return void
	 */
	public function actionEdit($id)
	{
		$model = $this->loadModel($id, 'Page');
		$data = array();
		$model->scenario = 'edit';
		if (isset($_POST['Page']))
		{
			$model->setAttributes($_POST['Page']);
			if ($model->save())
			{
				Yii::app()->user->setFlash('success', Yii::t('app', 'Your Edits are saved :)'));
				$this->actionView($id);
			}
			else
			{
				$data["model"] = $model;
				$this->renderPartial('_form', $data, false, true);
			}
		}
		else
		{
			$data["model"] = $model;
			$this->renderPartial('_form', $data, false, true);
		}
	}

	/**
	 * Take the page down
	 *
	 * First remove all page's followers and 2- & admins before deleting the page itself.
	 * If this page is the last one that its group holds then deleting
	 * it means that it's group pages_enabled option=0
	 *
	 * @param integer $id Id of the page to be deleted
	 * @author Asmaa
	 * @throws CHttpException For null model.
	 * @return void
	 */
	public function actionDelete($id)
	{
		// Only page admin can take a page down.
		if (PageAdmin::model()->isAdmin($id, Yii::app()->user->id))
		{

			$model = $this->loadModel($id, 'Page');
			$name = $model->name;
			if ($model === null)
			{
				throw new CHttpException(404, Yii::t('app', 'The requested page does not exist.'));
			}
			else
			{

				$groupId = $model->group_id;
				// 1. delete all of the page followers.
				if (UserFollowPage::model()->deleteAllFollowers($model->page_id)
				// 2. delete all of the page admins.
 && PageAdmin::model()->deleteAllAdminsOfPage($model->page_id)
				// 3. delete the page itself.
 && $model->delete())
				{
					Yii::app()->user
							  ->setFlash('success',
										 Yii::t('app', 'Page ') . $name
											. Yii::t('app', ' is deleted successfully'));
					$group = $this->loadModel($groupId, 'Group');
					// If this page is the last one that its group holds then deleting
					// it means that it's group pages_enabled option=0.
					if (count($group->pages) == 0)
					{
						$group->pages_enabled = 0;
						$group->save();
					}
					$this->redirect(array(
								'group/' . $groupId
							));
				}
				$this->redirect(array(
							'view', $id
						));
			}

		}
		else
		{
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
		}
	}

	/**
	 * User follows a page
	 *
	 * @param integer $pageId The followed page id
	 * @param integer $userId The follower user id
	 * @author Asmaa
	 * @return void
	 */
	public function actionFollowPage($pageId, $userId)
	{
		$model = new UserFollowPage();
		$model->following_user_id = $userId;
		$model->followed_page_id = $pageId;

		if ($model->save())
		{
			$data["pageId"] = $pageId;
			$data["isFollowing"] = UserFollowPage::model()
					->isFollowingPage($pageId, Yii::app()->user->id);
			$this->renderPartial('_followPage', $data, false, true);
		}
	}

	/**
	 * User unfollows a page
	 *
	 * @param integer $pageId The unfollowed page id
	 * @param integer $userId The follower user id
	 * @author Asmaa
	 * @throws CHttpException For null models.
	 * @return void
	 */
	public function actionUnfollowPage($pageId, $userId)
	{
		$model = UserFollowPage::model()
				->findByAttributes(array(
								   'followed_page_id' => $pageId, 'following_user_id' => $userId
								   ));
		if ($model === null)
		{
			throw new CHttpException(404, Yii::t('app', 'The requested page does not exist.'));
		}
		else
		{
			if ($model->delete())
			{
				$data["pageId"] = $pageId;
				$data["isFollowing"] = false;
				$this->renderPartial('_followPage', $data, false, true);
			}
		}
	}

	/**
	 * Yii default Index action
	 *
	 * @author Yii
	 * @return void
	 */
	public function actionIndex()
	{
		$dataProvider = new CActiveDataProvider('Page');
		$this->render('index', array(
					'dataProvider' => $dataProvider,
				));
	}

	/**
	 * Yii default Admin action
	 *
	 * @author Yii
	 * @return void
	 */
	public function actionAdmin()
	{
		$model = new Page('search');
		$model->unsetAttributes();

		if (isset($_GET['Page']))
		{
			$model->setAttributes($_GET['Page']);
		}

		$this->render('admin', array(
					'model' => $model,
				));
	}
}
