<?php
/**
 * Group Controller.
 *
 * @author  Asmaa
 * @version version:1.0
 * @since   version 1.0
 */

/**
 * Group Controller Class.
 *
 * @author  Asmaa
 * @version version:1.0
 * @since   version 1.0
 */
class GroupController extends GxController
{
	/**
	 * Filters
	 *
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations.
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 *
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
				array(
						'allow',
						// allow authenticated user to perform 'create' and 'update' actions.
						'actions' => array(
								'create',
								'update',
								'viewInvitePeople',
								'edit',
								'index',
								'view',
								'viewPartial',
								'viewPosts',
								'viewFiles',
								'viewMembers',
								'viewPhotos',
								'viewPages',
								'viewCalendar',
								'viewTasks',
								'viewRequests',
								'invite',
								'acceptInv',
								'declineInv',
								'approveInv',
								'cancelInv',
								'delete',
								'leaveGroup'
						),
						'users' => array(
							'@'
						),
				),
				array(
					'allow', // allow admin user to perform 'admin' and 'delete' actions.
					'actions' => array(
						'admin'
					), 'users' => array(
						'admin'
					),
				),
				array(
					'deny', // deny all users.
					'users' => array(
						'*'
					),
				),
		);
	}

	/**
	 * Registring scripts for the controller
	 *
	 * @author Asmaa
	 * @return void
	 */
	public function init()
	{
		Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/css/group.css');
		Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/group.js');
	}

	/**
	 * Rendering the group view and all of its related data
	 *
	 * @author Asmaa
	 * @param integer $id Id of the group to be viewed
	 * @throws CHttpException For not authenticated users.
	 * @return void
	 */
	public function actionView($id)
	{
		if (GroupMember::model()->isAuthToView(Yii::app()->user->id, $id))
		{
			$data = array();
			$data = Group::model()->getGroupStatistics($id);
			if (Yii::app()->getRequest()->getIsAjaxRequest())
			{
				$this->renderPartial('_groupView', $data, false, true);
			}
			else
			{
				$this->render('view', $data);
			}
		}
		else
		{
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
		}
	}

	/**
	 * Displays a particular group's posts.
	 *
	 * @param integer $id Id of the group to view its posts
	 * @author Asmaa
	 * @throws CHttpException For not authenticated users.
	 * @return void
	 */
	public function actionViewPosts($id)
	{
		if (GroupMember::model()->isAuthToView(Yii::app()->user->id, $id))
		{
			$data = array();
			$data["model"] = $this->loadModel($id, 'Group');
			$this->renderPartial('group-navigation/_groupPosts', $data, false, true);
		}
		else
		{
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
		}
	}

	/**
	 * Displays a particular group's files.
	 *
	 * @param integer $id Id of the group to view its files
	 * @author Asmaa
	 * @throws CHttpException For not authenticated users.
	 * @return void
	 */
	public function actionViewFiles($id)
	{
		if (GroupMember::model()->isAuthToView(Yii::app()->user->id, $id))
		{
			$data = array();
			$data["model"] = $this->loadModel($id, 'Group');
			$this->renderPartial('group-navigation/_groupFiles', $data, false, true);
		}
		else
		{
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
		}
	}

	/**
	 * Displays a particular group's members including the administrator.
	 *
	 * @param integer $id Id of the group to view its members
	 * @author Asmaa
	 * @todo get all members of the group inclusing admins
	 * @throws CHttpException For not authenticated users.
	 * @return void
	 */
	public function actionViewMembers($id)
	{
		if (GroupMember::model()->isAuthToView(Yii::app()->user->id, $id))
		{
			$data["members"] = GroupMember::model()->getGroupMembers($id);
			$data["groupId"] = $id;
			$this->renderPartial('group-navigation/_groupMembers', $data, false, true);
		}
		else
		{
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
		}
	}

	/**
	 * Displays a particular group's photo album.
	 *
	 * @param integer $id Id of the group to view its photos
	 * @author Asmaa
	 * @throws CHttpException For not authenticated users.
	 * @return void
	 */
	public function actionViewPhotos($id)
	{
		if (GroupMember::model()->isAuthToView(Yii::app()->user->id, $id))
		{
			$data = array();
			$data["model"] = $this->loadModel($id, 'Group');
			$this->renderPartial('group-navigation/_groupPhotos', $data, false, true);
		}
		else
		{
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
		}
	}

	/**
	 * Displays a particular group's pages.
	 *
	 * @param integer $id Id of the group to view its pages
	 * @author Asmaa
	 * @todo Get pages that belong to this group, this option is only available for premium
	 * subscribers
	 * @throws CHttpException For not authenticated users.
	 * @return void
	 */
	public function actionViewPages($id)
	{
		if (GroupMember::model()->isAuthToView(Yii::app()->user->id, $id))
		{
			$data = array();
			$data["pages"] = Group::model()->getGroupPages($id);
			$data["groupId"] = $id;
			$this->renderPartial('group-navigation/_groupPages', $data, false, true);
		}
		else
		{
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
		}
	}

	/**
	 * Displays a particular group's calendar.
	 *
	 * @param integer $id Id of the group to view its calendar
	 * @author Asmaa
	 * @throws CHttpException For not authenticated users.
	 * @return void
	 */
	public function actionViewCalendar($id)
	{
		if (GroupMember::model()->isAuthToView(Yii::app()->user->id, $id))
		{
			$data = array();
			$data["model"] = $this->loadModel($id, 'Group');
			$this->renderPartial('group-navigation/_groupCalendar', $data, false, true);
		}
		else
		{
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
		}

	}

	/**
	 * Displays a particular group's tasks.
	 *
	 * @param integer $id Id of the group to view its tasks
	 * @author Asmaa
	 * @throws CHttpException For not authenticated users.
	 * @return void
	 */
	public function actionViewTasks($id)
	{
		if (GroupMember::model()->isAuthToView(Yii::app()->user->id, $id))
		{
			$data = array();
			$data["model"] = $this->loadModel($id, 'Group');
			$this->renderPartial('group-navigation/_groupTasks', $data, false, true);
		}
		else
		{
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
		}
	}

	/**
	 * Displays a particular requests sent to the admin page 'group-navigation/_pendingRequests'
	 *
	 * @param integer $id Id of the group to view its pending requests
	 * @author Asmaa
	 * @todo get all pending invitations from the group_members table only until this version
	 * @throws CHttpException For not authenticated users.
	 * @return void
	 */
	public function actionViewRequests($id)
	{
		if (GroupMember::model()->isAuthToView(Yii::app()->user->id, $id))
		{
			$data = array();
			$data["groupId"] = $id;
			$pendingInv = GroupMember::model()->getPendingInvitations($id);
			$models = array();

			if (!($pendingInv == null))
			{
				foreach ($pendingInv as $req)
				{
					$models[] = User::model()->findByPk($req->user_id);
				}
			}
			$data["invReqs"] = $models;
			$this->renderPartial('group-navigation/_pendingRequests', $data, false, true);
		}
		else
		{
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
		}
	}

	/**
	 * Renders the view of inviting people for a particular group
	 *
	 * @param integer $id Id of the group to view its invite people form
	 * @author Asmaa
	 * @todo load all user models of people who the _logged in_ user is following and they
	 * are following him back to be changed on friendship relation
	 * @return void
	 */
	public function actionViewInvitePeople($id)
	{
		$model = $this->loadModel($id, 'Group');
		$data = array();
		$data["model"] = $model;
		$people = UserFollowUser::model()->getFollowingEachOther(Yii::app()->user->id);
		$data["people"] = $people;

		$this->renderPartial('_invitePeople', $data, false, true);
	}

	/**
	 * The logged in user accepts invitation and this updates his group membership
	 *
	 * Update the group membership of the logged-in user and set the pending field to 0 so
	 * he becomes a final group member
	 *
	 * @param integer $id Id of the group
	 * @author Asmaa
	 * @return void
	 */
	public function actionAcceptInv($id)
	{
		$userid = Yii::app()->user->id;
		$model = GroupMember::model()
				->findByAttributes(array(
					'group_id' => $id, 'user_id' => $userid
				));
		$model->pending = 0;
		if ($model->save())
		{
			Yii::app()->user->setFlash('success', Yii::t('app', 'Welcome! :) '));
			$this->redirect(array(
						'view', 'id' => $id
					));
		}
	}

	/**
	 * Invited user has the ability to decline invitations sent to him
	 * If declining is successful, the browser will be redirected to the 'site/view' page.
	 *
	 * @param integer $id Id of the group
	 * @author Asmaa
	 * @return void
	 */
	public function actionDeclineInv($id)
	{
		if ($this->actionCancelInv(Yii::app()->user->id, $id))
		{
			$this->redirect(array(
						'site/index'
					));
		}
	}

	/**
	 * Creating the invitations to join group denoted by $id to be sent to the user
	 *
	 * If the invitation request is created by admin the request is sent to the user directly,
	 * elsewise it will wait for admin approval
	 *
	 * @param integer $id Group id
	 * @author Asmaa
	 * @return void
	 */
	public function actionInvite($id)
	{
		// Invitations by the admin is approved automatically.
		$approved = (GroupMember::model()->isAdmin(Yii::app()->user->id, $id)) ? 1 : 0;
		// only user ids are sent.
		$invited = array_keys($_POST);

		$error = false;
		foreach ($invited as $i)
		{
			if (!GroupMember::model()->addMember($id, $i, 0, $approved))
			{
				$error = true;

			}
		}
		if (!$error)
		{
			$msg = ($approved)
					? Yii::t('app', 'Invitation is|Invitations are', count($invited))
							. Yii::t('app', ' sent')
					: Yii::t('app', 'Invitation is|Invitations are', count($invited))
							. Yii::t('app', ' awaiting admin\'s approval');
			Yii::app()->user->setFlash('success', $msg);
			$this->actionView($id);
		}
		else
		{
			$this->actionViewInvitePeople($id);
		}
	}

	/**
	 * A group member can leave any group he is member of
	 *
	 * If the user is not a group member, it will throw an exception
	 * otherwise the membership will be deleted and the user is redirected to the index page
	 *
	 * @param integer $groupId Group id
	 * @param integer $userId  Member(user) id
	 * @author Asmaa
	 * @throws CHttpException For not authenticated users.
	 * @return void
	 */
	public function leaveGroup($groupId, $userId)
	{
		$model = GroupMember::model()
				->findByAttributes(array(
					'group_id' => $groupId, 'user_id' => $userId
				));
		if ($model === null)
		{
			throw new CHttpException(404, Yii::t('app', 'The requested page does not exist.'));
		}
		elseif ($model->delete())
		{
			$this->redirect(array(
						'site/index'
					));
		}
	}

	/**
	 * Creates a new group.
	 *
	 * Get all group types, add the group and set the creator as an admin,
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 *
	 * @author Asmaa
	 * @return void
	 */
	public function actionCreate()
	{

		$model = new Group;
		$model->scenario = 'create';

		// Get all group types.
		$types = GroupType::model()->findAll();
		foreach ($types as $type)
		{
			$type->icon = GroupType::model()->getGroupTypeIconPath($type->icon);
		}
		// To select the first type automatically.
		$model->type_id = $types[0]->type_id;
		$this->performAjaxValidation($model);

		if (isset($_POST['Group']))
		{
			$model->attributes = $_POST['Group'];
			$model->creator_id = Yii::app()->user->id;
			if ($model->save())
			{
				// 1 add the creator as an admin.
				if (GroupMember::model()->addMember($model->group_id, $model->creator_id, 1, 1))
				{
					Yii::app()->user
							  ->setFlash('success',
										 Yii::t('app', 'VOILA :D !') . $model->name
											. Yii::t('app', '  is created successfully'));
					$this->redirect(array(
								'view', 'id' => $model->group_id
							));
				}
			}
		}
		$this
				->render('create',
						 array(
							'model' => $model, 'types' => $types,
						 ));
	}

	/**
	 * Editing some of the group data
	 *
	 * If the option pages_enabled isset then we'll create a page automatically
	 *
	 * @param integer $id Group id
	 * @author Asmaa
	 * @return void
	 */
	public function actionEdit($id)
	{
		$model = $this->loadModel($id, 'Group');
		$model->scenario = 'edit';
		$data = array();
		$data["model"] = $model;
		if (isset($_POST['Group']))
		{
			$model->attributes = $_POST['Group'];
			if (isset($_POST['Group']['pages_enabled']))
			{
				if (!Page::model()->addPage($model))
				{
					$model->pages_enabled = 0;
				}
			}
			if ($model->save())
			{
				Yii::app()->user->setFlash('success', Yii::t('app', 'Your Edits are saved :)'));
				$this->actionView($id);
			}
			else
			{
				$this->renderPartial('_form', $data, false, true);
			}
		}
		else
		{
			$this->renderPartial('_form', $data, false, true);
		}
	}
	/**
	 * Delete a group
	 *
	 * If the group has a page or more he cannot proceed as he needs to take them down first,
	 * else delete all of the group members first
	 *
	 * @param integer $id Group id
	 * @author Asmaa
	 * @throws CHttpException For not authenticated users.
	 * @return void
	 */
	public function actionDelete($id)
	{
		// Only an admin can elete a group.
		if (GroupMember::model()->isAdmin(Yii::app()->user->id, $id))
		{
			$model = $this->loadModel($id, 'Group');
			if ($model === null)
			{
				throw new CHttpException(404, Yii::t('app', 'The requested page does not exist.'));
			}
			else
			{
				// Check if the group has pages.
				if (count($model->pages) == 0)
				{
					$name = $model->name;
					if ((GroupMember::model()->deleteAllMembers($model->group_id))
							&& ($model->delete()))
					{
						Yii::app()->user
								  ->setFlash('success',
											 Yii::t('app', 'Group ') . $name
												. Yii::t('app', ' is deleted successfully'));
						$this->redirect(array(
									'site/index'
								));
					}
				}
				else
				{
					Yii::app()->user
							  ->setFlash('success',
										 Yii::t('app',
												"You have to take your page |pages ",
												count($model->pages))
											. Yii::t('app', "down before taking the group down"));
					$this->redirect(array(
								'view', 'id' => $id
							));
				}

			}
		}
		else
		{
			throw new CHttpException(400, Yii::t('app', 'Your request is invalid.'));
		}
	}
	/**
	 * Admin should approve invitations initiated by non-admin members
	 * in order to be sent to the target user
	 *
	 * @param integer $userId  The user to be set to this group
	 * @param integer $groupId The group id to set the admin to
	 * @author Asmaa
	 * @return boolean true if model saved correctly and false otherwise
	 */
	public function actionApproveInv($userId, $groupId)
	{
		$model = GroupMember::model()
				->findByAttributes(array(
					'user_id' => $userId, 'group_id' => $groupId
				));
		$model->approved_by_admin = 1;
		return $model->save();
	}

	/**
	 * Invited user and Admin has the ability of canceling invitations
	 *
	 * @param integer $userId  User id
	 * @param integer $groupId Group id
	 * @author Asmaa
	 * @return boolean true if the group member row is deleted successfully and false otherwise
	 */
	public function actionCancelInv($userId, $groupId)
	{
		$model = GroupMember::model()
				->findByAttributes(array(
					'user_id' => $userId, 'group_id' => $groupId
				));
		if ($model === null)
		{
			return false;
		}
		else
		{
			return $model->delete();
		}
	}

	/**
	 * List all of the groups
	 *
	 * @author Yii
	 * @return void
	 */
	public function actionIndex()
	{
		$dataProvider = new CActiveDataProvider('Group');
		$this->render('index', array(
					'dataProvider' => $dataProvider,
				));
	}

	/**
	 * Yii default Admin action
	 *
	 * @author Yii
	 * @return void
	 */
	public function actionAdmin()
	{
		$model = new Group('search');
		$model->unsetAttributes();

		if (isset($_GET['Group']))
		{
			$model->setAttributes($_GET['Group']);
		}

		$this->render('admin', array(
					'model' => $model,
				));
	}

}
