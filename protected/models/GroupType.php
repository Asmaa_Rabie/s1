<?php
/**
 * Group Type Model.
 *
 * @author  Asmaa
 * @version version:1.0
 * @since   version 1.0
 */

Yii::import('application.models._base.BaseGroupType');

/**
 * Group Type Model Class.
 *
 * @author  Asmaa
 * @version version:1.0
 * @since   version 1.0
 */
class GroupType extends BaseGroupType
{
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}
	/**
	 * Gets the group type icon full path
	 *
	 * @param string $icon Icon's name
	 * @author Asmaa
	 * @return string path of the icon
	 */
	public function getGroupTypeIconPath($icon)
	{
		return Yii::app()->baseUrl . Config::model()->getConfigValue('group_type_icon') . $icon;
	}

}
