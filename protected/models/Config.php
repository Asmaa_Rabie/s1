<?php
/**
 * Configuration Model.
 *
 * @author  Asmaa
 * @version version:1.0
 * @since   version 1.0
 */

Yii::import('application.models._base.BaseConfig');

/**
 * Configuration Model Class.
 *
 * @author  Asmaa
 * @version version:1.0
 * @since   version 1.0
 */
class Config extends BaseConfig
{

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * Get the configuration value like a path of a folder
	 *
	 * @param integer $pk The primary key of the value
	 * @author Asmaa
	 * @return string
	 */
	public function getConfigValue($pk)
	{
		return Config::model()->findByAttributes(array(
							  'config_name' => $pk
							  ));
	}

}
