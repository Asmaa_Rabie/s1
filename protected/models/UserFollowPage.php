<?php
/**
 * User follow page Model.
 *
 * @author  Asmaa
 * @version version:1.0
 * @since   version 1.0
 */

Yii::import('application.models._base.BaseUserFollowPage');

/**
 * User follow page Model Class.
 *
 * @author  Asmaa
 * @version version:1.0
 * @since   version 1.0
 */
class UserFollowPage extends BaseUserFollowPage
{

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * Checks if user $userId is following the page $pageId
	 *
	 * @param integer $pageId Page id
	 * @param integer $userId User id
	 * @return boolean true if the user is following the page and false otherwise
	 * @author Asmaa
	 */
	public function isFollowingPage($pageId, $userId)
	{
		return (!(UserFollowPage::model()
						 ->findByAttributes(array(
											'followed_page_id' => $pageId,
											'following_user_id' => $userId
											))) == null);
	}

	/**
	 * Returns all the followers of page $id
	 *
	 * @param integer $pageId Page id
	 * @author Asmaa
	 * @return array of user models
	 */
	public function getPageFollowers($pageId)
	{
		$followers = array();
		$r = UserFollowPage::model()->findAllByAttributes(array(
									'followed_page_id' => $pageId
									));
		foreach ($r as $follower)
		{
			$model = User::model()->findByPk($follower["following_user_id"]);
			$model->profile_picture = User::model()->getUserAvatar($model->profile_picture);
			$followers[] = $model;
		}
		return $followers;
	}

	/**
	 * Delete all followers of a page
	 *
	 * @param integer $pageId Page id
	 * @author Asmaa
	 * @return boolean true if all followers are deleted successfully and false otherwise
	 */
	public function deleteAllFollowers($pageId)
	{
		if ($this->findAllByAttributes(array(
					 'followed_page_id' => $pageId
					 )) != null)
		return ($this->deleteAllByAttributes(array(
					 'followed_page_id' => $pageId
					 )));
		else return true;
	}

	/**
	 * Sets creation date before saving the model
	 *
	 * @author Asmaa
	 * @return parent::beforeSave()
	 */
	public function beforeSave()
	{
		if ($this->isNewRecord)
		{
			$this->creation_date = time();
		}
		return parent::beforeSave();
	}

}
