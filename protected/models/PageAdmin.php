<?php
/**
 * Page Admin Model.
 *
 * @author  Asmaa
 * @version version:1.0
 * @since   version 1.0
 */

Yii::import('application.models._base.BasePageAdmin');

/**
 * Page Admin Model Class.
 *
 * @author  Asmaa
 * @version version:1.0
 * @since   version 1.0
 */
class PageAdmin extends BasePageAdmin
{

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * Check if a user is an admin of a specific page
	 *
	 * @param integer $pageId Page id
	 * @param integer $userId User id
	 * @return boolean true if the user is the admin of the page and false otherwise
	 * @author Asmaa
	 */
	public function isAdmin($pageId, $userId)
	{
		return (!(PageAdmin::model()
						 ->findByAttributes(array(
											'admin_id' => $userId, 'page_id' => $pageId
											))) == null);
	}

	/**
	 * Get all admins of the page
	 *
	 * @param integer $pageId Page id
	 * @return array of user models
	 * @author Asmaa
	 */
	public function getPageAdmins($pageId)
	{
		$r = PageAdmin::model()
				->findAllByAttributes(array(
					'page_id' => $pageId
				));
		$admins = array();
		foreach ($r as $admin)
		{
			$model = User::model()->findByPk($admin["admin_id"]);
			$model->profile_picture = User::model()->getUserAvatar($model->profile_picture);
			$admins[] = $model;
		}
		return $admins;
	}

	/**
	 * Add an admin to the page
	 *
	 * @param integer $pageId Page id
	 * @param integer $userId User id
	 * @author Asmaa
	 * @return boolean true if the user is added successfully and false otherwise
	 */
	public function addAdmin($pageId, $userId)
	{
		$admin = new PageAdmin();
		$admin->page_id = $pageId;
		$admin->admin_id = $userId;
		return ($admin->save());
	}

	/**
	 * Delete all admins of a page
	 *
	 * @param integer $pageId Page id
	 * @author Asmaa
	 * @return boolean true if all admins are deleted successfully and false otherwise
	 */
	public function deleteAllAdminsOfPage($pageId)
	{
		if ($this->deleteAllByAttributes(array(
					 'page_id' => $pageId
					 )) != null)
		return ($this->deleteAllByAttributes(array(
					 'page_id' => $pageId
					 )));
		else return true;
	}

	/**
	 * Sets creation/ last edit dates before saving the model
	 *
	 * @author Asmaa
	 * @return parent::beforeSave()
	 */
	public function beforeSave()
	{
		if ($this->isNewRecord)
		{
			$this->creation_date = time();

		}
		return parent::beforeSave();
	}
}
