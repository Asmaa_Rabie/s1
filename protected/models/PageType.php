<?php
/**
 * Page Type Model.
 *
 * @author  Asmaa
 * @version version:1.0
 * @since   version 1.0
 */

Yii::import('application.models._base.BasePageType');

/**
 * Page Type Model Class.
 *
 * @author  Asmaa
 * @version version:1.0
 * @since   version 1.0
 */
class PageType extends BasePageType
{
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * Gets the page type icon full path
	 *
	 * @param string $icon Icon's name
	 * @author Asmaa
	 * @return string path of the icon
	 */
	public function getPageTypeIconPath($icon)
	{
		return Yii::app()->baseUrl . Config::model()->getConfigValue('page_type_icon') . $icon;
	}

}
