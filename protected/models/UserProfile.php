<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author SherifMedhat
 * @version version: 1.0
 * @since Release 1.0
 *
 * Form of registration
 */
class UserProfile extends User
{

	public $rememberMe;
	private $_identity;

	/**
	 * @return array validation rules for model attributes.
	 * @author SherifMedhat
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.

		$rules = array(
		array('first_name, last_name, email, password, last_access_date, join_date, type_id', 'required','on'=>'register'),
		//email and password are required
		array('email, password', 'required','on'=>'login'),
		// rememberMe needs to be a boolean
		array('rememberMe', 'boolean'),
		array('email', 'unique','on'=>'register'),
	 // when in login scenario, password must be authenticated
		array('password', 'authenticate', 'on'=>'login'),
		);
		return array_merge(parent::rules(), $rules);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 * @author SherifMedhat
	 */
	public function attributeLabels()
	{
		$attributeLabels = array(
		'rememberMe'=>Yii::t('test', 'Remember me next time')
		);
		return array_merge(parent::attributeLabels(), $attributeLabels);
	}

	/**
	 * Authenticates the password.
	 * This is the 'authenticate' validator as declared in rules().
	 */
	public function authenticate($attribute,$params)
	{
		$this->_identity=new UserIdentity($this->email,$this->password);
		if(!$this->_identity->authenticate())
		$this->addError('password','Incorrect email or password.');
	}


	/**
	 * Logs in the user using the given email and password in the model.
	 * @return boolean whether login is successful
	 */
	public function login($isAfterRegistration=false)
	{
		if($this->_identity===null)
		{
			$this->_identity=new UserIdentity($this->email,$this->password);
			if($isAfterRegistration){
				$this->_identity->storeUserDetails($this);
			}else{
					
				$this->_identity->authenticate();
			}
		}
		if($this->_identity->errorCode===UserIdentity::ERROR_NONE)
		{
			$duration=$this->rememberMe ? 3600*24*30 : 0; // 30 days
			Yii::app()->user->login($this->_identity,$duration);
			return true;
		}
		//else
		echo $this->_identity->errorCode;
		return false;
	}

	public function register(){
		$this->salt = $this->generateSalt();
		$this->password = $this->hashPassword($this->password, $this->salt);
		return $this->save(false);

	}

	public function fillFromSocialProfile($userSocialProfile){

		$this->first_name = $userSocialProfile->firstName;
		$this->last_name = $userSocialProfile->lastName;
		$this->email = $userSocialProfile->email;
		$this->profile_picture = $userSocialProfile->photoURL;
		$this->info = $userSocialProfile->description;
	}

}

?>
