<?php
/**
 * User follow user Model.
 *
 * @author  Asmaa
 * @version version:1.0
 * @since   version 1.0
 */

Yii::import('application.models._base.BaseUserFollowUser');

/**
 * User follow user Model Class.
 *
 * @author  Asmaa
 * @version version:1.0
 * @since   version 1.0
 */
class UserFollowUser extends BaseUserFollowUser
{

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * Checks whether the logged in user is following the user defined by $id
	 *
	 * @param integer $id User id
	 * @author Asmaa
	 * @return boolean true if the logged in user is following $id and false otherwise
	 */
	public function isFollowingUser($id)
	{
		$following = Yii::app()->user->id;
		return (!(UserFollowUser::model()
				->findByAttributes(array(
								   'following_user_id' => $following, 'followed_user_id' => $id
								   ))) == null);
	}

	/**
	 * Gets all the users who are following the user defined by $followed
	 *
	 * @param integer $followed User id
	 * @author Asmaa
	 * @return array of users
	 */
	public function getFollowersOf($followed)
	{
		return UserFollowUser::model()
				->findAllByAttributes(array(
									  'followed_user_id' => $followed
									  ));
	}

	/**
	 * Gets all the users who are the user defined by $following is following
	 *
	 * @param integer $following User id
	 * @author Asmaa
	 * @return array of user models
	 */
	public function getFollowedBy($following)
	{
		return UserFollowUser::model()
				->findAllByAttributes(array(
					'following_user_id' => $following
				));
	}

	/**
	 * Gets follow-follow back users
	 *
	 * @param integer $id User id to check for his followers
	 * @author Asmaa
	 * @return array of user models
	 */
	public function getFollowingEachOther($id)
	{
		$feo = array();
		$following = $this->getFollowedBy($id);
		if (!($following == null))
		{
			foreach ($following as $f)
			{
				if (!(UserFollowUser::model()
						->findByAttributes(array(
										   'following_user_id' => $f->followed_user_id,
										'followed_user_id' => $id
										   ))) == null)
				{
					$model = User::model()->findByPk($f->followed_user_id);
					$model->profile_picture = User::model()->getUserAvatar($model->profile_picture);
					$feo[] = $model;
				}
			}
		}
		return $feo;
	}
	/**
	 * Sets creation date before saving the model
	 *
	 * @author Asmaa
	 * @return parent::beforeSave()
	 */
	public function beforeSave()
	{
		if ($this->isNewRecord)
		{
			$this->creation_date = time();
		}
		return parent::beforeSave();
	}

}
