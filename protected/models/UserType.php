<?php

Yii::import('application.models._base.BaseUserType');

class UserType extends BaseUserType
{
	Const BASIC_USER=1;
	Const STUDENT =2;
	Const TEACHER_ASSISTANT=3;
	Const DOCTOR=4;
	Const GRADUATE=5;


	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	/**
	 * @author SherifMedhat
	 * @return a list of user types options ex:student, doctor,,,,
	 */
	public function getUserTypes()
	{
		return array(
		UserType::BASIC_USER => Yii::t('app', 'Basic User'),
		UserType::STUDENT => Yii::t('app', 'Student'),
		UserType::TEACHER_ASSISTANT => Yii::t('app', 'Teacher Assistant'),
		UserType::DOCTOR => Yii::t('app', 'Doctor'),
		UserType::GRADUATE => Yii::t('app', 'Graduate'),
		);
	}
}