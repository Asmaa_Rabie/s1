<?php
/**
 * Group Member Model.
 *
 * @author  Asmaa
 * @version version:1.0
 * @since   version 1.0
 */

Yii::import('application.models._base.BaseGroupMember');

/**
 * Group Member Model Class.
 *
 * @author  Asmaa
 * @version version:1.0
 * @since   version 1.0
 */
class GroupMember extends BaseGroupMember
{

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * Checks if a user is invited to a group
	 *
	 * When pending=1 and approved_by_admin=1 this means that invitation is sent to the $userId
	 *
	 * @param integer $userId  User id
	 * @param integer $groupId Group id
	 * @author Asmaa
	 * @return boolean true if the user is invited and false otherwise
	 */
	public function isInvited($userId, $groupId)
	{
		return (!(GroupMember::model() ->findByAttributes(array(
											'user_id' => $userId,
											'group_id' => $groupId,
											'pending' => 1,
											'approved_by_admin' => 1
											))) == null) ? true : false;
	}

	/**
	 * Checks if a user is a member of a group
	 *
	 * @param integer $userId  User id
	 * @param integer $groupId Group id
	 * @author Asmaa
	 * @return boolean true if the user is a member of the group and false otherwise
	 */
	public function isMember($userId, $groupId)
	{
		return (!(GroupMember::model()
						 ->findByAttributes(array(
											'user_id' => $userId,
											'group_id' => $groupId,
											'pending' => 0
											))) == null) ? true : false;
	}

	/**
	 * Checks if a user is an admin of a specific group
	 *
	 * @param integer $userId  User id
	 * @param integer $groupId Group id
	 * @author Asmaa
	 * @return boolean true if the user is an admin of the group and false otherwise
	 */
	public function isAdmin($userId, $groupId)
	{
		return (!(GroupMember::model()
						 ->findByAttributes(array(
											'user_id' => $userId,
											'group_id' => $groupId,
											'admin' => 1
											))) == null) ? true : false;
	}

	/**
	 * Checks if the invitation of a user to join the group is waiting the admin's approval
	 *
	 * @param integer $userId  User id
	 * @param integer $groupId Group id
	 * @author Asmaa
	 * @return boolean true if an invitation to join the group is waiting the admin's
	 *  approval and false otherwise
	 */
	public function isWaitingApproval($userId, $groupId)
	{
		return (!(GroupMember::model()
						 ->findByAttributes(array(
											'user_id' => $userId,
											'group_id' => $groupId,
											'approved_by_admin' => 0,
											'pending' => 1
											))) == null) ? true : false;
	}

	/**
	 * Checks if a user is authorized to see group content
	 * group members, admins and invited people can view this content
	 *
	 * @param integer $userId  User id
	 * @param integer $groupId Group id
	 * @author Asmaa
	 * @return boolean true if the user is authorized to view, and false otherwise
	 */
	public function isAuthToView($userId, $groupId)
	{
		return (!(GroupMember::model()
						 ->findByAttributes(array(
											'user_id' => $userId,
											'group_id' => $groupId,
											'approved_by_admin' => 1
											))) == null) ? true : false;
	}

	/**
	 * Gets invitations pending for admin's approval
	 *
	 * @param integer $groupId Group id
	 * @author Asmaa
	 * @return array of group member models
	 */
	public function getPendingInvitations($groupId)
	{
		return GroupMember::model()
				->findAllByAttributes(array(
					'group_id' => $groupId, 'approved_by_admin' => 0
				));
	}

	/**
	 * Gets group members
	 *
	 * @param integer $groupId Group id
	 * @author Asmaa
	 * @return array of user models
	 */
	public function getGroupMembers($groupId)
	{
		$members = array();
		$r = GroupMember::model()
				->findAllByAttributes(array(
					'group_id' => $groupId, 'pending' => 0
				));
		foreach ($r as $member)
		{
			$model = User::model()->findByPk($member["user_id"]);
			$model->profile_picture = User::model()->getUserAvatar($model->profile_picture);
			$members[] = $model;
		}
		return $members;
	}

	/**
	 * Adds a member to a group
	 *
	 * @param integer $groupId  Group id
	 * @param integer $userId   User id
	 * @param boolean $admin    True if the add request initiated by an admin
	 * @param boolean $approved True if the add request is approved by an admin
	 * @author Asmaa
	 * @return boolean true if model saved correctly and false otherwise
	 */
	public function addMember($groupId, $userId, $admin, $approved)
	{

		$member = new GroupMember();
		$member->group_id = $groupId;
		$member->user_id = $userId;
		$member->admin = $admin;
		$member->approved_by_admin = $approved;
		$member->pending = ($admin == true) ? 0 : 1;
		return ($member->save());
	}

	/**
	 * Sets a member of the group as admin
	 *
	 * @param integer $groupId The group id to set the admin to
	 * @param integer $userId  The user to be set to this group
	 * @author Asmaa
	 * @return boolean true if model saved correctly and false otherwise
	 */
	public function setAdmin($groupId, $userId)
	{
		$admin = GroupMember::model()
				->findByAttributes(array(
					'group_id' => $groupId, 'user_id' => $userId
				));
		$admin->pending = 0;
		$admin->admin = 1;
		$admin->approved_by_admin = 1;
		return ($admin->save());
	}

	/**
	 * Delete all group members
	 *
	 * @param integer $groupId Group id
	 * @author Asmaa
	 * @return boolean true if all members are deleted successfully and false otherwise
	 */
	public function deleteAllMembers($groupId)
	{
		return $this->deleteAllByAttributes(array(
					'group_id' => $groupId
					));
	}

	/**
	 * Sets creation/ last edit dates before saving the model
	 *
	 * @author Asmaa
	 * @return parent::beforeSave()
	 */
	public function beforeSave()
	{
		if ($this->isNewRecord)
		{
			$this->join_date = time();
		}
		$this->last_edit_date = time();

		return parent::beforeSave();
	}

}
