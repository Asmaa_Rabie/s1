<?php
/**
 * Page Model.
 *
 * @author  Asmaa
 * @version version:1.0
 * @since   version 1.0
 */

Yii::import('application.models._base.BasePage');

/**
 * Page Model Class.
 *
 * @author  Asmaa
 * @version version:1.0
 * @since   version 1.0
 */
class Page extends BasePage
{

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * Gets all page followers
	 *
	 * @param integer $pageId Page id
	 * @author Asmaa
	 * @return array of user models
	 */
	public function getPageFollowers($pageId)
	{
		$r = UserFollowPage::model()->getAllByAttributes(array(
									'followed_page_id' => $pageId
									));
		$followers = array();
		foreach ($r as $follower)
		{
			$followers[] = $follower->users;
		}
		return $followers;
	}

	/**
	 * Gets the page's icon full path
	 *
	 * @param string $icon Icon's name
	 * @author Asmaa
	 * @return string path of the icon
	 */
	public function getPageIcon($icon)
	{
		$path = '';
		if (isset($icon) && $icon != null)
		{
			$path = Yii::app()->baseUrl . Config::model()->getConfigValue('page_icon_path') . $icon;
		}
		else
		{
			$path = Yii::app()->baseUrl . Config::model()->getConfigValue('page_default_icon');
		}
		return $path;
	}

	/**
	 * Gets the page's cover full path
	 *
	 * @param string $cover Cover's name
	 * @author Asmaa
	 * @return string path of the cover
	 */
	public function getPageCover($cover)
	{
		$path = '';
		if (isset($cover) && $cover != null)
		{
			$path = Yii::app()->baseUrl . Config::model()->getConfigValue('page_cover_path')
					. $cover;
		}
		else
		{
			$path = Yii::app()->baseUrl . Config::model()->getConfigValue('page_default_cover');
		}
		return $path;
	}

	/**
	 * Add a page (like create page but independent from the creation form)
	 *
	 * @param object $group Object of the model group
	 * @author Asmaa
	 * @return boolean true if a page and its admin are added successfully and false otherwise
	 */
	public function addPage($group)
	{
		$model = new Page();
		$model->creator_id = $group['creator_id'];
		$model->group_id = $group['group_id'];
		$model->type_id = $group['type_id'];
		$model->name = $group['name'];
		$model->info = $group['info'];
		if ($model->save())
		{
			return PageAdmin::model()->addAdmin($model->page_id, $model->creator_id);
		}
		else
		{
			return false;
		}
	}
	/**
	 * Sets creation/ last edit dates before saving the model
	 *
	 * @author Asmaa
	 * @return parent::beforeSave()
	 */
	public function beforeSave()
	{
		if ($this->isNewRecord)
		{
			$this->creation_date = time();
		}
		$this->last_edit_date = time();
		return parent::beforeSave();
	}
}
