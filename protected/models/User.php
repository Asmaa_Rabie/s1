<?php

Yii::import('application.models._base.BaseUser');

class User extends BaseUser
{
    Const MALE=1;
    Const FEMALE=2;

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Checks if the given password is correct.
     * @param string the password to be validated
     * @return boolean whether the password is valid
     */
    public function validatePassword($password)
    {
        return $this->hashPassword($password, $this->salt) === $this->password;
    }

    /**
     * Generates the password hash.
     * @param string password
     * @param string salt
     * @return string hash
     */
    public function hashPassword($password, $salt)
    {
        return md5($salt . $password);
    }

    /**
     * Generates a salt that can be used to generate a password hash.
     * @return string the salt
     */
    protected function generateSalt()
    {
        return uniqid('', true);
    }

    /**
     * @author SherifMedhat
     * @return a list of user types options ex:student, doctor,,,,
     */
    public function getGenderOptions()
    {
        return array(
            User::MALE => Yii::t('app', 'Male'),
            User::FEMALE => Yii::t('app', 'Female'),
        );
    }

    public function getFullName()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function getSuggest($q)
    {
        $c = new CDbCriteria();
        $c->addSearchCondition('first_name', $q, true, 'OR');
        $c->addSearchCondition('last_name', $q, true, 'OR');
        $c->addSearchCondition('email', $q, true, 'OR');
        return $this->findAll($c);
    }

    /**
     * gets the user avatar's full path 
     * @param type $cover
     * @return string 
     * @author asmaa
     */
    public function getUserAvatar($avatar)
    {
        $path = '';
        if (isset($avatar) && $avatar != null)
            $path = Yii::app()->baseUrl . Config::model()->getConfigValue('user_avatar_path') . $avatar;
        else
            $path = Yii::app()->baseUrl . Config::model()->getConfigValue('user_default_avatar');
        return $path;
    }

    /**
     * Gets all groups the logged in user is administrating
     * @return array of group models
     * @author Asmaa
     */
    public function getAdminGroups()
    {
        $id = Yii::app()->user->id;
        $result = Yii::app()->db->createCommand(array(
                    'select' => 'g.group_id',
                    'from' => 'group as g , group_member as gt',
                    'where' => "g.group_id = gt.group_id and gt.user_id=$id and gt.admin=1",
                ))->queryAll();
        foreach ($result as $d)
        {
            $data[] = Group::model()->findByPk($d["group_id"]);
        }
        return $data;
    }

}