<?php
/**
 * Group Model.
 *
 * @author  Asmaa
 * @version version:1.0
 * @since   version 1.0
 */

Yii::import('application.models._base.BaseGroup');

/**
 * Group Model Class.
 *
 * @author  Asmaa
 * @version version:1.0
 * @since   version 1.0
 */
class Group extends BaseGroup
{

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * Gets the group icon full path
	 *
	 * @param string $icon ]con's name
	 * @author Asmaa
	 * @return string path of the icon
	 */
	public function getGroupIcon($icon)
	{
		$path = '';
		if (isset($icon) && $icon != null)
		{
			$path = Yii::app()->baseUrl . Config::model()->getConfigValue('group_icon_path')
					. $icon;
		}
		else
		{
			$path = Yii::app()->baseUrl . Config::model()->getConfigValue('group_default_icon');
		}
		return $path;
	}

	/**
	 * Gets the group cover full path
	 *
	 * @param string $cover Cover's name
	 * @author Asmaa
	 * @return string path of the cover
	 */
	public function getGroupCover($cover)
	{
		$path = '';
		if (isset($cover) && $cover != null)
		{
			$path = Yii::app()->baseUrl . Config::model()->getConfigValue('group_cover_path')
					. $cover;
		}
		else
		{
			$path = Yii::app()->baseUrl . Config::model()->getConfigValue('group_default_cover');
		}
		return $path;
	}

	/**
	 * Returns all Pages this group holds -A feature for premium subscribers only-
	 *
	 * @param integer $groupId Group id
	 * @author Asmaa
	 * @return array of pages this group holds
	 */
	public function getGroupPages($groupId)
	{
		$r = Page::model()->findAllByAttributes(array(
						  'group_id' => $groupId
						  ));
		$pages = array();

		foreach ($r as $page)
		{
			$model = Page::model()->findByPk($page["page_id"]);
			$model->icon = Page::model()->getPageIcon($model->icon);
			$model->cover = Page::model()->getPageCover($model->cover);
			$pages[] = $model;
		}

		return $pages;
	}
	/**
	 * Displays a particular groups statistical data.
	 *
	 * @todo to change the filesCount and postsCount
	 * @param integer $id The ID of the group model to be displayed
	 * @author Asmaa
	 * @return array of data holding the model, its type, # of files, # of posts and #of members
	 */
	public function getGroupStatistics($id)
	{
		$data = array();
		$data["model"] = Group::model()->findByPk($id);
		$data["model"]->icon = Group::model()->getGroupIcon($data["model"]->icon);
		$data["model"]->cover = Group::model()->getGroupCover($data["model"]->cover);
		$data["type"] = $data["model"]->type;
		$criteria = new CDbCriteria();
		$criteria->condition = "group_id= $id AND pending=0";
		$data["filesCount"] = 5;
		$data["postsCount"] = 90;
		$data["membersCount"] = count(GroupMember::model()->findAll($criteria));
		return $data;
	}

	/**
	 * Get the group name
	 *
	 * @param integer $groupId Group id
	 * @return string
	 * @author Asmaa
	 */
	public function getGroupName($groupId)
	{
		return GxHtml::encode(GxHtml::valueEx(Group::model()->findByPk($groupId)));
	}

	/**
	 * Sets creation/ last edit dates before saving the model
	 *
	 * @author Asmaa
	 * @return parent::beforeSave()
	 */
	public function beforeSave()
	{
		if ($this->isNewRecord)
		{
			$this->creation_date = time();
		}
		$this->last_edit_date = time();

		return parent::beforeSave();
	}
}
